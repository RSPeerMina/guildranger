import org.rspeer.runetek.adapter.Interactable;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.commons.StopWatch;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.*;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.runetek.event.listeners.ItemTableListener;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.ItemTableEvent;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.script.Script;
import org.rspeer.script.ScriptCategory;
import org.rspeer.script.ScriptMeta;
import org.rspeer.ui.Log;
import org.rspeer.runetek.api.component.Dialog;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.function.Predicate;

@ScriptMeta(
        name = "GuildRanger",
        desc = "Plays the minigame at Ranging guild for XP and Tickets ",
        developer = "Mina",
        category = ScriptCategory.MINIGAME)

public class GuildRanger extends Script implements RenderListener, ItemTableListener {

    private int START_XP = 0;
    private int START_TICKET = 0;
    private int CURRENT_TICKET = 0;
    private int mode = 3;
    private JButton normal;
    private JButton insane;
    StopWatch timer;

    private static final Predicate<String> ENTER = a -> a.contains("give it a");

    public void onStart() {
        START_XP = Skills.getExperience(Skill.RANGED);

        if (Inventory.getCount(true, "Archery ticket") > 0) {
            START_TICKET = Inventory.getCount(true, "Archery ticket");
        }

        timer = StopWatch.start();
        GUI();
    }

    public void GUI() {
        JFrame jFrame = new JFrame("Speed");

        jFrame.setSize(300, 100);
        jFrame.setResizable(false);
        jFrame.setLocationRelativeTo(Game.getCanvas());

        jFrame.setLayout(new FlowLayout());

        normal = new JButton("Normal");
        insane = new JButton("Insane");

        jFrame.add(normal);
        jFrame.add(insane);

        jFrame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        jFrame.pack();

        jFrame.setVisible(true);

        normal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Log.fine("[SPEED] Normal");
                mode = 0;
                jFrame.setVisible(false);
            }
        });

        insane.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Log.fine("[SPEED] Insane");
                mode = 1;
                jFrame.setVisible(false);
            }
        });
    }

    public int loop() {

        if (mode == 3) {
            Log.fine("Choose speed");
        } else {
            Interactable a = Inventory.getFirst("Bronze arrow");
            Npc j = Npcs.getNearest("Competition Judge");
            SceneObject t = SceneObjects.getNearest("Target");
            Position g = new Position(2673, 3417);

            if (g.distance() >= 5) {
                Movement.walkTo(g);
                Time.sleep(1000, 1250);
            }

            if (Combat.getAttackStyle() != Combat.AttackStyle.ACCURATE) {
                Combat.select(0);
                Log.info("Attack style is now ACCURATE");
            }

            if (EquipmentSlot.QUIVER.getItemStackSize() >= 1) {
                if (t != null) {
                    t.interact("Fire-at");
                    switch (mode) {
                        case 0:
                            Time.sleep(1000, 1250);
                            break;
                        case 1:
                            Time.sleep(100, 200);
                            break;
                    }

                }
            } else {
                if (a != null) {
                    a.interact("Wield");
                } else {
                    if (Dialog.canContinue()) {
                        if (Dialog.processContinue()) {
                            switch (mode) {
                                case 0:
                                    Time.sleep(1000, 1250);
                                    break;
                                case 1:
                                    Time.sleep(200, 300);
                                    break;
                            }
                        }
                    } else {
                        int enterLength = Interfaces.filterByText(ENTER).length;

                        if (enterLength > 0) {
                            if (Dialog.process(0)) {
                                switch (mode) {
                                    case 0:
                                        Time.sleep(1000, 1250);
                                        break;
                                    case 1:
                                        Time.sleep(200, 300);
                                        break;
                                }
                            }
                        } else {
                            if (j != null) {
                                if (Inventory.getCount(true, "Coins") >= 200) {
                                    j.interact("Talk-to");
                                } else {
                                    Log.severe("Out of coins!");
                                    setStopping(true);
                                }
                            }
                        }
                    }
                }
            }
        }
        return 500;
    }

    public void notify(ItemTableEvent e) {
        if (e.getChangeType() == ItemTableEvent.ChangeType.ITEM_ADDED && e.getId() == 1464) {
            CURRENT_TICKET = Inventory.getCount(true, "Archery ticket") - START_TICKET;
        }
    }


    @Override
    public void notify(RenderEvent e) {
        Graphics g = e.getSource();
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        int y = 35;
        int x = 10;
        g2.setColor(Color.GREEN);
        g2.drawString("GuildRanger by Mina", x, y);
        g2.drawString("Runtime: " + timer.toElapsedString(), x, y += 20);

        int rangeXP = Skills.getExperience(Skill.RANGED) - START_XP;
        if (rangeXP > 0) {
            g2.drawString("XP: " + rangeXP + " (" + (int) timer.getHourlyRate(rangeXP) / 1000 + "k/H)", x, y += 20);
        }

        int ticket = Inventory.getCount(true, "Archery ticket") - START_TICKET;
        if (ticket > 0) {
            g2.drawString("Tickets earned: " + ticket + " (" + (int) timer.getHourlyRate(ticket) / 1000 + "k/H)", x, y += 20);
        }
    }

    public void onStop() {
        Log.fine("Thank you for using GuildRanger by Mina!");
        Game.logout();
        setStopping(true);
    }
}
